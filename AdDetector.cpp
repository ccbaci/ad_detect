#include "AdDetector.h"

AdDetector::AdDetector()
{
#if API_WITH_OCR
    ocr = new tesseract::TessBaseAPI();
    ocr->Init(NULL, "eng");
    ocr->SetVariable("tessedit_char_whitelist", "REKLAM");
#endif
}

AdDetector::~AdDetector()
{
#if API_WITH_OCR
    ocr->End();
    delete ocr;
#endif
}

void AdDetector::addRoi(const cv::Rect & roi)
{
    rois.push_back(roi);
}

void AdDetector::addTemplate(AVFrame * temp_yuv, const bool &isStart)
{
    cv::Mat matrix = convertYUV420toGRAY(temp_yuv);
    addTemplate(matrix, isStart);
}

void AdDetector::addTemplate(const cv::Mat & temp, const bool & isStart)
{
    AdTemplate adTemp;

    // save identification
    adTemp.isStart = isStart;

    // save as grayscale
    if(temp.channels() == 3) cv::cvtColor(temp, adTemp.matrix, cv::COLOR_BGR2GRAY);
    else adTemp.matrix = temp.clone();

    templates.push_back(adTemp);
}

#if API_WITH_OCR
void AdDetector::addKeyword(std::string keyword)
{
    keywords.push_back(keyword);
}
#endif
#include <opencv2/opencv.hpp>
#include <iostream>
void AdDetector::getAd(const cv::Mat &frame, const int & method, bool & adFound, bool & isStart)
{
    adFound = false; isStart = false;
    for(uint i = 0; i < rois.size(); ++i) {
        cv::Rect roi(rois[i]);
        cv::Mat roi_color = frame(roi);
        cv::Mat roi_gray;

        if(roi_color.channels() == 3) cv::cvtColor(roi_color, roi_gray, cv::COLOR_BGR2GRAY);
        else roi_gray = roi_color;

        if(method & AdMethodTemplateMatching) {
            for(uint j = 0; j < templates.size(); ++j) {
                cv::Mat result;
                int result_cols = roi_gray.cols - templates[j].matrix.cols + 1;
                int result_rows = roi_gray.rows - templates[j].matrix.rows + 1;
                result.create(result_rows, result_cols, CV_32FC1);

                cv::matchTemplate(roi_gray, templates[j].matrix, result, cv::TM_CCORR_NORMED);
                double minVal; double maxVal; cv::Point minLoc; cv::Point maxLoc;
                minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc);

                bool start = templates[j].isStart && maxVal >= TEMPLATE_CONFIDENCE_THRESHOLD;
                bool end = !templates[j].isStart && maxVal >= TEMPLATE_CONFIDENCE_THRESHOLD;

                if(start) { adFound = true; isStart = true; return; }
                else if(end) { adFound = true; isStart = false; return; }

            }
        }
#if API_WITH_OCR
        if(method & AdMethodOcr) {
            // threshold
            cv::threshold(roi_gray, roi_gray, 127, 255, cv::THRESH_OTSU);

            // swt trial for faster ocr
                        {
                            roi_gray /= 255;
                            cv::Mat bw32f, swt32f, swt8u, kernel;
                            double min, max;
                            int strokeRadius;
                            roi_gray.convertTo(bw32f, CV_32F);  // format conversion for multiplication
                            cv::distanceTransform(roi_gray, swt32f, CV_DIST_L2, 5); // distance transform
                            cv::minMaxLoc(swt32f, &min, &max);  // find max
                            strokeRadius = (int)ceil(max);  // half the max stroke width
                            kernel = getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)); // 3x3 kernel used to select 8-connected neighbors
                            for(int r = 0; r < strokeRadius; r++) {
                                cv::dilate(swt32f, swt32f, kernel); // assign the max in 3x3 neighborhood to each center pixel
                                swt32f = swt32f.mul(bw32f); // apply mask to restore original shape and to avoid unnecessary max propogation
                            }
                            swt32f.convertTo(swt8u, CV_8U, 255.0, -255. * min /(max-min));
                            roi_gray = swt8u;
                        }

            ocr->SetImage((uchar*)roi_gray.data, roi_gray.size().width, roi_gray.size().height, roi_gray.channels(), roi_gray.step1());
            ocr->Recognize(0);
            tesseract::ResultIterator * ri = ocr->GetIterator();
            if(NULL != ri) {
                do {
                    char * word = ri->GetUTF8Text(tesseract::RIL_WORD);
                    if(!word) continue;
                    std::string text = word;
                    delete[] word;

                    // check confidence
                    float confidence = ri->Confidence(tesseract::RIL_WORD);
                    if(confidence < 100. * OCR_CONFIDENCE_THRESHOLD) continue;

                    // check keyword
                    bool isKeyword = false;
                    for(int k = 0; k < keywords.size(); ++k) {
                        if(text == keywords[k]) {
                            isKeyword = true;
                            break;
                        }
                    }
                    if(!isKeyword) continue;

                    // confidence and keyword OK
                    adFound = true;

                    // find if start or end
                    int x1, y1, x2, y2;
                    ri->BoundingBox(tesseract::RIL_WORD, &x1, &y1, &x2, &y2);
                    cv::Mat roi_word = roi_gray(cv::Rect(x1, y1, x2 - x1, y2 - y1));
                    double mean = cv::mean(roi_word)[0];
                    std::cout << text << " " << mean << std::endl;
                    isStart = (mean > OCR_COLOR_THRESHOLD);
                    return;

                } while (ri->Next(tesseract::RIL_WORD));
            }
        }
#endif
    }
}

void AdDetector::getAd(AVFrame * frame_yuv, const int &method, bool &adFound, bool &isStart)
{
    cv::Mat matrix = convertYUV420toGRAY(frame_yuv);
    return getAd(matrix, method, adFound, isStart);
}

cv::Mat AdDetector::convertYUV420toGRAY(AVFrame * frame_yuv)
{
    cv::Mat matrix = cv::Mat(frame_yuv->height, frame_yuv->width, CV_8UC1, frame_yuv->data[0]);
    return matrix;
}

