#include "AdDetector.h"

#include <chrono>
#include <iostream>
using namespace std;

#include <opencv2/opencv.hpp>
#include <decoder.h>

#define INPUT_VIDEO "/home/canberk/Desktop/out3.mp4"

int main(int argc, char *argv[])
{
    // ffmpeg decoder to get yuv frames from a video file
    av_register_all();
    avcodec_register_all();
    avformat_network_init();
    av_log_set_level(AV_LOG_ERROR);
    Decoder decoder;
    decoder.out_rgb = 0;
    decoder.address = INPUT_VIDEO;
    decoder.open();

    // init ad detector
    AdDetector detector;

    // add some rois
    float w = decoder.frame_video->width; float h = decoder.frame_video->height;
  //  detector.addRoi(cv::Rect(0, 0, w * .25, h * .150));
  //  detector.addRoi(cv::Rect(h * .5, 0, w * .25, h * .150));
    detector.addRoi(cv::Rect(w * .80, 0, w * .18, h * .18));

    // add template images
    cv::Mat start_temp = cv::imread("/home/canberk/Desktop/Persona/start_temp_1.jpg", cv::IMREAD_GRAYSCALE);
    cv::Mat start_temp2 = cv::imread("/home/canberk/Desktop/Persona/start_temp_2.jpg", cv::IMREAD_GRAYSCALE);
    cv::Mat end_temp2 = cv::imread("/home/canberk/Desktop/Persona/end_temp_2.jpg", cv::IMREAD_GRAYSCALE);
    cv::Mat end_temp = cv::imread("/home/canberk/Desktop/Persona/end_temp_1.jpg", cv::IMREAD_GRAYSCALE);
    detector.addTemplate(start_temp, true); // true means template is "start"
   // detector.addTemplate(start_temp2, true);
    detector.addTemplate(end_temp, false);
   // detector.addTemplate(end_temp2, false);

#if API_WITH_OCR
    detector.addKeyword("REKLAM");
    detector.addKeyword("REKLAMLAR");
#endif

    std::chrono::time_point<std::chrono::system_clock> start;

    int n = 0;
    do {
        n++;
        start = std::chrono::system_clock::now();
        int ret = decoder.getVideoFrame();
        if(ret < 0) break;

        // get if there is an ad in yuv frame
        bool adFound, isStart;
        //int method;
        //n % 24 ? method =  AdMethodTemplateMatching : method = AdMethodTemplateMatching | AdMethodOcr;

        int method = AdMethodTemplateMatching;
        detector.getAd(decoder.frame_video, method, adFound, isStart);

        if(adFound) {
            std::cout << "frame: " << n << " adFound. isStart: " << isStart << endl;
        }
        else {
            std::cout << "frame: " << n << " no ad." << endl;
        }
        std::chrono::duration<double> elapsed = std::chrono::system_clock::now() - start;
        std::cout << "elapsed time(ms): " << (int) (elapsed.count() * 1000.) << endl;

    } while(1);
    std::cout << "frame: " << n;
    return 0;

}
