#ifndef ADDETECTOR_H
#define ADDETECTOR_H

#define API_WITH_OCR 0

#if API_WITH_OCR
// sudo apt-get install libtesseract-dev libleptonica-dev
// sudo apt-get install tesseract-ocr-eng tesseract-ocr-tur
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>
#define OCR_CONFIDENCE_THRESHOLD 0.50
#define OCR_COLOR_THRESHOLD 127
#endif

// build from source
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

// ffmpeg
extern "C" {
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/imgutils.h>
}

#define TEMPLATE_CONFIDENCE_THRESHOLD 0.9

struct AdTemplate {
    cv::Mat matrix;
    bool isStart;
    std::vector<cv::Mat> scales;
};

enum AdMethod {
    AdMethodTemplateMatching = 1,
#if API_WITH_OCR
    AdMethodOcr = 2
#endif
};

class AdDetector
{
public:
    explicit AdDetector();
    ~AdDetector();

    // set roi(s)
    void addRoi(const cv::Rect & roi);

    // set template images with their identifications
    void addTemplate(AVFrame * temp_yuv, const bool & isStart);
    void addTemplate(const cv::Mat & temp, const bool & isStart);

    // get if there is an ad signal found within the preset rois of given frame, using given method
    void getAd(AVFrame * frame_yuv, const int & method, bool & adFound, bool & isStart);
    void getAd(const cv::Mat & frame, const int & method, bool & adFound, bool & isStart);

    // AVFrame YUV420 to grayscale cv::Mat
    cv::Mat convertYUV420toGRAY(AVFrame * frame_yuv);

private:
    std::vector<cv::Rect> rois;
    std::vector<AdTemplate> templates;

#if API_WITH_OCR
public:
    // set ocr keywords
    void addKeyword(std::string keyword);

private:
    tesseract::TessBaseAPI * ocr;
    std::vector<std::string> keywords;
#endif
};

#endif // ADDETECTOR_H
